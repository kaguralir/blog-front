import 'dotenv-flow/config.js';

import express from 'express';
import { configurePassport } from './utils/token.js';
import passport from 'passport';
import multer from 'multer';


import cors from 'cors';

import { UserController } from './controllers/UserController.js';
import { PostController } from './controllers/PostController.js';
import { CommentController } from './controllers/CommentController.js';

import bodyParser from 'body-parser';
import { RatingController } from './controllers/RatingController.js';




configurePassport();



export const server = express();


server.use(express.json({ extended: false }));
server.use(passport.initialize());
server.use(express.json());
server.use(cors());
server.use(express.static('public'));



server.use('/api/v1/user', UserController);
server.use('/api/v1/upload', PostController);
server.use('/api/v1/comment', CommentController);
server.use('/api/v1/rating', RatingController);


server.listen(process.env.PORT || 3001, (req, res) => {
  console.log("Server running on 3001");
});

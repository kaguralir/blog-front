export class CommentEntity {
    id;
    description;
    author;
    date;
    users_id;
    uploads_id;

    constructor(id, description, author, date, users_id, uploads_id){
        this.id= id;
        this.description = description;
        this.author = author;
        this.date = date;
        this.users_id= users_id;
        this.uploads_id = uploads_id;
    }
}

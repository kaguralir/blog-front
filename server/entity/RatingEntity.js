export class RatingEntity {
    id;
    ratingNumber;
    users_id;
    uploads_id ;
    /**
     * 
     * @param {number} id 
     * @param {number} ratingNumber 
     * @param {number} users_id 
     * @param {number} uploads_id
     */
    constructor(id, ratingNumber,users_id, uploads_id){
        this.id = id;
        this.ratingNumber = ratingNumber;
        this.users_id = users_id;
        this.uploads_id = uploads_id;

    }
}

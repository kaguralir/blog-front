

export class PostEntity {
    id;
    title;
    description;
    image;
    author;
    likes;
    author_id;
    /**
     * 
     * @param {number} id 
     * @param {string} title 
     * @param {string} description 
     * @param {string} image
     * @param {string} author 
     * @param {number} likes 
     * @param {number}  author_id
     */
    constructor(id,title, description, image, author, likes,  author_id){
        this.id = id;
        this.title = title;
        this.description = description;
        this.image = image;
        this.author = author;
        this.likes = likes;
        this. author_id =  author_id;


    }
}

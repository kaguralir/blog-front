
import fs from 'fs';
import jwt from 'jsonwebtoken';
import passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { getUser, addUser } from "../repositories/UserRepository.js";

const privateKey = fs.readFileSync('config/id_rsa');
const publicKey = fs.readFileSync('config/id_rsa.pub');

/**
 * @param {object} payload
 * @returns jwt
 */
export function generateToken(payload) {
    const token = jwt.sign(payload, privateKey, { algorithm: 'RS256', expiresIn: 60 * 60 });
    return token;
}


export function configurePassport() {
    passport.use(new Strategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: publicKey,
        algorithms: ['RS256']
    }, async (payload, done) => {

        try {
            const user = await getUser(payload.username);

            if (user) {

                return done(null, user);
            }

            return done(null, false);
        } catch (error) {
            console.log(error);
            return done(error, false);
        }
    }))

}

import { RatingEntity } from "../entity/RatingEntity.js";
import { connection } from "../config/db.js";



export async function postRating(ratingNumber,users_id, uploads_id) {
    console.log("rating is" ,ratingNumber);
    console.log("user is ", users_id);
    console.log("post is ",uploads_id);

    /*     console.log(users_id, uploads_id);
     */    await connection.query(
    
            "INSERT INTO Ratings (ratingNumber, users_id, uploads_id) VALUES (?, ?,?)",
            [ratingNumber, users_id, uploads_id]
        );
    /*     await connection.execute('UPDATE Uploads SET likes=(select COUNT(*) from Likes WHERE uploads_id=?) WHERE id=?', [uploads_id, uploads_id]);
     */}
    

export async function getAllRatings() {
    const [rows] = await connection.query(
        "SELECT * FROM Ratings"); 
    const Ratings = [];
    for (const row of rows) {
        let instance = new RatingEntity(row.id, row.ratingNumber,row.users_id, row.uploads_id);
        Ratings.push(instance);

    }

    return Ratings;

}





export async function deleteRating(id) {
    await connection.query('DELETE FROM Ratings WHERE id=?', [id]);
}

/* export async function getRatingsByUser(users_id) {
    const [rows] = await connection.execute('SELECT * FROM Ratings WHERE users_id=?', [users_id]);
    return rows.map(item => new RatingEntity(item['id'], item['ratingNumber']));
} */
export async function openRating(id) {
    let [row] = await connection.execute(`SELECT * FROM Ratings WHERE id=?`, [id]);
    return new RatingEntity(row[0]['id'], row[0]['ratingNumber'], row[0]['users_id'], row[0]['uploads_id']); //to check who noted 1 only for example
}



export async function findRatesByUser(ratingNumber, userId, uploads_id) {

    const [row] = await connection.execute('SELECT * FROM Ratings WHERE ratingNumber=? users_id=? AND uploads_id=?', [ratingNumber, userId, uploads_id])
    return row[0]
}


export async function getRatingsByUser(users_id) {
    const [rows] = await connection.execute('SELECT * FROM Ratings WHERE users_id=?', [users_id]);
    return rows.map(item => new RatingEntity(item['id'], item['ratingNumber'], item['users_id'], item['uploads_id']));
}





export async function updateRating(rate){
    await connection.query('UPDATE Ratings SET ratingNumber=? WHERE id=?', [rate.ratingNumber, rate.id]);
};


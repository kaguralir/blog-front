import { PostEntity } from "../entity/PostEntity.js";
import { connection } from "../config/db.js";



export async function postUpload(post, user) {


    const [rows] = await connection.query(
        "INSERT INTO Uploads (title, description, image, author, author_id) VALUES (?, ?, ?, ?, ?)",
        [post.title, post.description, post.image, user.username, user.id]);
    post.id = rows.insertId;


}


export async function getAllPosts() {
    const [rows] = await connection.query(
        "SELECT * FROM Uploads");
    const posts = [];
    for (const row of rows) {
        let instance = new PostEntity(row.id, row.title, row.description, row.image, row.author, row.likes);
        posts.push(instance);

    }

    return posts;

}




export async function postLikes(users_id, uploads_id) {
    console.log(users_id, uploads_id);
    await connection.query(

        "INSERT INTO Likes (users_id, uploads_id) VALUES (?,?)",
        [users_id, uploads_id]
    );
    await connection.execute('UPDATE Uploads SET likes=(select COUNT(*) from Likes WHERE uploads_id=?) WHERE id=?', [uploads_id, uploads_id]);
}


export async function deleteLike(users_id, uploads_id) {
    console.log(users_id, uploads_id);
    await connection.execute('DELETE FROM Likes WHERE users_id=? AND uploads_id=?', [users_id, uploads_id]);
    await connection.execute('UPDATE Uploads SET likes=(select COUNT(*) from Likes WHERE uploads_id=?) WHERE id=?', [uploads_id, uploads_id])
}
// select * from post left join post_like on post.id=post_like.post_id



/* export async function findLikesByUsers(user) {
    const [rows] = await connection.query('SELECT * FROM Likes WHERE users_id=?', [user]);
    if(rows.length === 1) {
        return new PostEntity(row.id, row.users_id, row.uploads_id);
    }
    return null;

} */

/* 
export async function openPost(id) {
    const [rows] = await connection.query(
        "SELECT * FROM Uploads WHERE id = ?", [id]);
        const post = [];
        for (const row of rows) {
            let instance = new PostEntity(row.id, row.title, row.description, row.image, row.author);
            post.push(instance);
    
        }

    return post;


}

export async function getPostsByUser(id) {
    const [rows] = await connection.query(
        "SELECT * FROM Uploads WHERE author_id = ?", [id]);
        const posts = [];
        for (const row of rows) {
            let instance = new PostEntity(row.id, row.title, row.description, row.image, row.author, row.author_id);
            posts.push(instance);
    
        }

    return posts;


} */

export async function deletePost(id) {
    await connection.query('DELETE FROM Uploads WHERE id=?', [id]);
}

export async function getPostsByUser(userId) {
    const [rows] = await connection.execute('SELECT * FROM Uploads WHERE author_id=?', [userId]);
    return rows.map(item => new PostEntity(item['id'], item['title'], item['description'], item['image'], item['author']));
}
export async function openPost(id) {
    let [row] = await connection.execute(`SELECT * FROM Uploads WHERE id=?`, [id]);
    return new PostEntity(row[0]['id'], row[0]['title'], row[0]['description'], row[0]['image'], row[0]['author']);
}

export async function findLikesByUsers(userId, postId) {
    console.log(userId, postId);

    const [row] = await connection.execute('SELECT * FROM Likes WHERE users_id=? AND uploads_id=?', [userId, postId])
    return row[0]
}








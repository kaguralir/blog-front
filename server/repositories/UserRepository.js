import { UserEntity } from "../entity/UserEntity.js";
import { connection } from "../config/db.js";





export async function getUser(username) {
    const [rows] = await connection.query('SELECT * FROM Users WHERE username=?', [username]);
    if (rows.length === 1) {
        return new UserEntity(rows[0].id, rows[0].username, rows[0].password);
    }
    return null;

}



export async function addUser(user) {
    const [rows] = await connection.query('INSERT INTO Users (username, password) VALUES (?,?)', [user.username, user.password]);
    user.id = rows.insertId;
}


export async function getAllUsers() {
    const [rows] = await connection.execute('SELECT * FROM Users');


    const users = [];
    for (const row of rows) {
        let instance = new UserEntity(row.id, row.username, row.password);
        users.push(instance);

    }
    return users;

}





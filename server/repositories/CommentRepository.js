import { CommentEntity } from "../entity/CommentEntity.js";
import { connection } from "../config/db.js";



export async function postComment(comment) {


    const [rows] = await connection.query('INSERT INTO Comments ( description, author, users_id, uploads_id) VALUES (?,?,?,?)',
        [comment.description, comment.author, comment.users_id, comment.uploads_id]);
    comment.id = rows.insertId;

}


export async function getAllcomments() {
    const [rows] = await connection.query(
        "SELECT * FROM Comments"); 
    const comments = [];
    for (const row of rows) {
        let instance = new CommentEntity(row.id, row.description, row.author,row.date, row.users_id, row.uploads_id);
        comments.push(instance);

    }

    return comments;

}





export async function deleteComment(id) {
    await connection.query('DELETE FROM Comments WHERE id=?', [id]);
}

export async function getCommentsByUser(users_id) {
    const [rows] = await connection.execute('SELECT * FROM Comments WHERE users_id=?', [users_id]);
    return rows.map(item => new CommentEntity(item['id'], item['description'], item['author'], item['date']));
}
export async function openComment(id) {
    let [row] = await connection.execute(`SELECT * FROM Comments WHERE id=?`, [id]);
    return new CommentEntity(row[0]['id'], row[0]['description'], row[0]['author'], row[0]['date']);
}










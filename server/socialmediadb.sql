DROP TABLE IF EXISTS Users;
CREATE TABLE Users(
  id int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
  username VARCHAR (255) NOT NULL UNIQUE,
  password VARCHAR (255) NOT NULL
) default charset utf8 comment '';
DROP TABLE IF EXISTS Likes;
CREATE TABLE Likes(
  id int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
  users_id INT (255),
  FOREIGN KEY (users_id) REFERENCES Users(id),
  uploads_id INT (255),
  FOREIGN KEY (uploads_id) REFERENCES Uploads(id)
) default charset utf8 comment '';
DROP TABLE IF EXISTS Uploads;
CREATE TABLE Uploads(
  id int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
  title VARCHAR (255) NOT NULL,
  description VARCHAR (1000) NOT NULL,
  image VARCHAR (255),
  likes INT (250) DEFAULT (0),
  author_id INT (255),
  FOREIGN KEY (author_id) REFERENCES Users(id),
  comments_id INT (255),
  FOREIGN KEY (comments_id) REFERENCES Uploads(id)
) default charset utf8 comment '';
ALTER TABLE
  Uploads
ADD
  author VARCHAR (250)
AFTER
  image;
DROP TABLE IF EXISTS Ratings;
CREATE TABLE Ratings(
    id int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    rate INT (255),
    users_id INT (255),
    FOREIGN KEY (users_id) REFERENCES Users(id),
    uploads_id INT (255),
    FOREIGN KEY (uploads_id) REFERENCES Uploads(id)
  ) default charset utf8 comment '';
DROP TABLE IF EXISTS Comments;
CREATE TABLE Comments(
    id int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    description VARCHAR (255),
    author VARCHAR(255),
    date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    users_id INT (255),
    FOREIGN KEY (users_id) REFERENCES Users(id),
    uploads_id INT (255),
    FOREIGN KEY (uploads_id) REFERENCES Uploads(id)
  ) default charset utf8 comment '';

DROP TABLE IF EXISTS Ratings;
CREATE TABLE Ratings(
    id int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    ratingNumber INT(255),
    users_id INT (255),
    FOREIGN KEY (users_id) REFERENCES Users(id),
    uploads_id INT (255),
    FOREIGN KEY (uploads_id) REFERENCES Uploads(id)
  ) default charset utf8 comment '';
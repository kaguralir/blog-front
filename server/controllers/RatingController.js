import { Router } from "express";
import passport from "passport";
import { deleteRating, getRatingsByUser, openRating, getAllRatings, postRating, findRatesByUser, updateRating } from "../repositories/RatingRepository.js";
import { RatingEntity } from "../entity/RatingEntity.js";
export const RatingController = Router();

RatingController.get('/', async (req, res) => {
    try {
        const post = await getAllRatings();

        return res.status(200).json({
            success: true,
            count: post.length,
            data: post
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({
            success: false,
            error: 'Server Error'
        });
    }
});

RatingController.post('/', passport.authenticate('jwt', { session: false }), async (req, resp) => {
    try {
        const ratingNumber= req.body.ratingNumber;
        const users_id = req.user.id;
        const uploads_id = req.body.uploads_id;
        console.log('rating is', ratingNumber, 'users id is', users_id,'uploads_id', uploads_id)

        const ratedAlreay = await findRatesByUser(users_id, uploads_id);
        if (!ratedAlreay) {
            const postRate = await postRating(ratingNumber,users_id, uploads_id);

            return resp.status(200).json({
                success: true,
                count: postRate,
                data: postRate
            });
        }
/*         else {
            const postRate = await updateRating(ratingNumber, users_id, uploads_id);

            return resp.status(200).json({
                success: true,
                count: postRate,
                data: postRate
            });
        }
 */



    } catch (err) {
        console.log(err);
        return resp.status(500).json({
            success: false,
            error: 'Server Error'
        });
    }

})


RatingController.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let toDelete = await deleteRating(req.params.id);
        res.status(202).end();

    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

RatingController.get('/byRating/:id', passport.authenticate('jwt', { session: false }), async (req, resp) => {
    try {
        let post = await openRating(req.params.id);
        resp.status(200).json(post);
    }
    catch (err) {
        console.log(err);
        console.log(err.data)
        resp.status(404).json({ error: 'No post' });

    }
})

RatingController.get('/byUser/:id', passport.authenticate('jwt', { session: false }), async (req, resp) => {
    try {
        const user = await getRatingsByUser(req.user.id);

        resp.status(200).json(user);
    }
    catch (err) {
        console.log(err)
        resp.status(404).json({ error: 'Not Found' });
    }
})
import { Router } from "express";
import { postUpload, getAllPosts, getPostsByUser, postLikes, openPost, findLikesByUsers, deletePost, deleteLike } from "../repositories/PostRepository.js";
import passport from "passport";
import { PostEntity } from "../entity/PostEntity.js";
export const PostController = Router();
import multer from "multer";
import { uploader } from "../src/uploaders.js";




PostController.post('/', passport.authenticate('jwt', { session: false }), uploader, async (req, res) => {
    console.log('body', req.body);
    console.log('reqfile', req.file);


    try {

        let postEntity = new PostEntity();
        Object.assign(postEntity, req.body);
        postEntity.image = '/uploads/' + req.file.filename;
        await postUpload(postEntity, req.user);


        res.status(201).json(postEntity);

    } catch (err) {
        console.log(err);
        if (err.name === 'ValidationError') {
            const messages = Object.values(err.errors).map(val => val.message);

            return res.status(400).json({
                success: false,
                error: messages
            });
        } else {
            return res.status(500).json({
                success: false,
                error: 'Server Error'
            });
        }
    }
})


// @desc    Get all posts
// @route   GET /api/v1/posts
// @access  Public
PostController.get('/', async (req, res) => {
    try {
        const post = await getAllPosts();

        return res.status(200).json({
            success: true,
            count: post.length,
            data: post
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({
            success: false,
            error: 'Server Error'
        });
    }
});






// @desc    Get post by user
// @route   Get /api/v1/uploads/byUser/:id
// @access  Public
PostController.get('/byUser/:id', passport.authenticate('jwt', { session: false }), async (req, resp) => {
    try {
        const user = await getPostsByUser(req.params.id);

        resp.status(200).json(user);
    }
    catch (err) {
        console.log(err)
        resp.status(404).json({ error: 'Not Found' });
    }
})

// @desc    Get post by id
// @route   Get /api/v1/byUser/:id
// @access  Public
PostController.get('/byPost/:id', passport.authenticate('jwt', { session: false }), async (req, resp) => {
    try {
        let post = await openPost(req.params.id);
        resp.status(200).json(post);
    }
    catch (err) {
        console.log(err);
        console.log(err.data)
        resp.status(404).json({ error: 'No post' });

    }
})


// @desc    Post like 
// @route   Get /api/v1/upload/like
// @access  Public
PostController.post('/like', passport.authenticate('jwt', { session: false }), async (req, resp) => {
    try {
        const users_id = req.user.id;
        const uploads_id = req.body.uploads_id;


        const likedAlready = await findLikesByUsers(users_id, uploads_id);
        if (!likedAlready) {
            const postLike = await postLikes(users_id, uploads_id);

            return resp.status(200).json({
                success: true,
                count: postLike,
                data: postLike
            });
        }
        else {
            const postLike = await deleteLike(users_id, uploads_id);

            return resp.status(200).json({
                success: true,
                count: postLike,
                data: postLike
            });
        }




    } catch (err) {
        console.log(err);
        return resp.status(500).json({
            success: false,
            error: 'Server Error'
        });
    }

})





PostController.delete("/:id", passport.authenticate('jwt', { session: false }), async (req, res) => {

    await deletePost(req.params.id);

    res.json("DELETED SUCCESSFULLY");
});


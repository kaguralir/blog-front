import { Router } from "express";
import { UserEntity } from "../entity/UserEntity.js";
import { getUser, addUser, getAllUsers } from "../repositories/UserRepository.js";
import bcrypt from 'bcrypt';
import { generateToken } from "../utils/token.js";
import passport from "passport";
import { configurePassport } from "../utils/token.js"



export const UserController = Router();





// @desc    Get all users
// @route   GET /api/v1/users
// @access  Public
UserController.get('/allUsers', async (req, res) => {
    try {
        const post = await getAllUsers();

        return res.status(200).json({
            success: true,
            count: post.length,
            data: post
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({
            success: false,
            error: 'Server Error'
        });
    }
});


// @desc    Add user
// @route   POST /api/v1/user/register
// @access  Public


UserController.post('/register', async (req, res) => {
    try {
        const newUser = new UserEntity();
        Object.assign(newUser, req.body);

        const exists = await getUser(newUser.username);
        if (exists) {
            res.status(400).json({ error: 'Username already taken' });
            return;
        }

        newUser.password = await bcrypt.hash(newUser.password, 11);

        await addUser(newUser);


        res.status(201).json(newUser);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

// @desc    Login
// @route   GET /api/v1/users/login
// @access  Public

UserController.post('/login', async (req, res) => {
    try {

        const user = await getUser(req.body.username);
        console.log(user);
        if (user) {
            const samePassword = await bcrypt.compare(req.body.password, user.password);
            console.log(samePassword);
            if (samePassword) {
                res.json({
                    loggedIn: true,
                    user,
                    token: generateToken({
                        id: user.id,
                        username: user.username
                    })
                });
                return;
            }
        }
        res.status(401).json({ loggedIn: false, error: 'Wrong username and/or password' });


    } catch (error) {
        console.log(error);
        res.status(500).json({ loggedIn: false, error });
    }
});



UserController.get('/account', passport.authenticate('jwt', { session: false }), (req, res) => {

    res.json(req.user);
});




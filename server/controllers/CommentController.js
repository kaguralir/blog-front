import { Router } from "express";
import passport from "passport";
import { deleteComment, getCommentsByUser, openComment, getAllcomments, postComment } from "../repositories/CommentRepository.js";
import { CommentEntity } from "../entity/CommentEntity.js";
export const CommentController = Router();

CommentController.get('/', async (req, res) => {
    try {
        const post = await getAllcomments();

        return res.status(200).json({
            success: true,
            count: post.length,
            data: post
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({
            success: false,
            error: 'Server Error'
        });
    }
});

CommentController.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
/*     console.log(CommentEntity)
 */    try {
        let newComment = new CommentEntity();
        Object.assign(newComment, req.body);
/*         console.log(newComment)
 */     newComment.author = req.user.username;
        newComment.users_id = req.user.id;

        await postComment(newComment);

        res.status(201).json(newComment);
        console.log(newComment)


    } catch (err) {
        console.log(err);
        if (err.name === 'ValidationError') {
            const messages = Object.values(err.errors).map(val => val.message);

            return res.status(400).json({
                success: false,
                error: messages
            });
        } else {
            return res.status(500).json({
                success: false,
                error: 'Server Error'
            });
        }
    }
});



CommentController.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let toDelete = await deleteComment(req.params.id);
        res.status(202).end();

    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

CommentController.get('/byComment/:id', passport.authenticate('jwt', { session: false }), async (req, resp) => {
    try {
        let post = await openComment(req.params.id);
        resp.status(200).json(post);
    }
    catch (err) {
        console.log(err);
        console.log(err.data)
        resp.status(404).json({ error: 'No post' });

    }
})

CommentController.get('/byUser/:id', passport.authenticate('jwt', { session: false }), async (req, resp) => {
    try {
        const user = await getCommentsByUser(req.params.id);

        resp.status(200).json(user);
    }
    catch (err) {
        console.log(err)
        resp.status(404).json({ error: 'Not Found' });
    }
})
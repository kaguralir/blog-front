# This project is about a blog that allows people to post and delete post, register, like and unlike, and comment and delete comment.

# This project is focused on the concept of registration, and login, using token JWT, to protect the routes that allow people to access the site.

The routes are protected with JWT, and you must be logged to access some functionalities.

The backend was created with MySQL (controllers, repositories, entities), express and cors.

The front was created with react, and some libraries such as antd for small icons.

### `npm install` `npm install`

Use npm install first to install all modules necessary to run the app.

### Example of post


*![alt text](https://gitlab.com/kaguralir/blog-front/-/raw/master/Assets/post.png)


Here an example of what a post looks like. You can see that you can like, rate, and add a title, a description and a picture also.

### Example of uploading a post, page

*![alt text](https://gitlab.com/kaguralir/blog-front/-/raw/master/Assets/createapostpage.png)

Here is page that shows how you can create a post. This page has a similar design for registration and login.

### Example of comment page

*![alt text](https://gitlab.com/kaguralir/blog-front/-/raw/master/Assets/fullcommentsectionexemple.png)
*![alt text](https://gitlab.com/kaguralir/blog-front/-/raw/master/Assets/anotherexemple.png)


### The app is also responsive, with images downloaded being able to fit whatever screen you use

*![alt text](https://gitlab.com/kaguralir/blog-front/-/raw/master/Assets/sharkresponsive.png)

# Test the site here

Runs the app in the development mode.\
Open [https://blog-app-hedwig.herokuapp.com/](https://blog-app-hedwig.herokuapp.com/) test, only requirement is to register and login first.


# THANK YOU !

# This project is about a blog that allows people to post and delete post, register, like and unlike, and comment and delete comment

The routes are protected with JWT, and you must be logged to access some functionalities.

The backend was created with MySQL (controllers, repositories, entities), express and cors.


### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### Example of uploading page

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Example of registration page

In the project directory, you can run:

## Example of comment page

In the project directory, you can run:

import axios from "axios";

export class AuthService {
  static async register(user) {
    try {
      const response = await axios.post(
        "https://blog-app-back-hedwig.herokuapp.com/api/v1/user/register",
        user
      );

      return response.data;
    } catch (err) {
      console.log(err);
      console.log(err.response.data);
    }
  }

  static async login(user) {
    const response = await axios.post(
      "https://blog-app-back-hedwig.herokuapp.com/api/v1/user/login",
      user
    );
    localStorage.setItem("token", response.data.token);

    return response.data.user;
  }

  static async fetchAccount() {
    const response = await axios.get(
      "https://blog-app-back-hedwig.herokuapp.com/api/v1/user/account"
    );

    return response.data;
  }
}

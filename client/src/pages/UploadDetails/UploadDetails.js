import React, { useEffect, useState, useContext } from "react";
import { useParams, useHistory } from "react-router-dom";
import "./UploadDetails.css";
import { useDispatch, useSelector } from "react-redux";
import { deleteApost, DeleteThePost, fetchAllPosts } from "../../stores/AllPosts";
import { fetchPostById } from "../../stores/PostById";
import { postCom } from "../../stores/PostCom";
import { deleteAcomment, DeleteTheCom, fetchAllComms } from "../../stores/AllComs";



function UploadDetails() {
    let { id } = useParams();

    const dispatch = useDispatch();
    const PostById = useSelector((state) => state.uploadsByIdStore.uploadsByIdState);
    const loggedIn = useSelector(state => state.userStore.user);
    const allcomentary = useSelector(state => state.gettingAllCommentary.commentsTotalState);
    console.log(id);


      


    const [description, setNewComment] = useState("");

    useEffect(() => {
        dispatch(fetchPostById(id));



    }, [id]);


    const author = loggedIn.username;
    const user = loggedIn.id;


    const postingCom = () => {
        dispatch(postCom({
            description: description,
            author: author,
            users_id: user,
            uploads_id: id
        }));
        console.log(description, loggedIn.username, loggedIn.id, id)
        console.log(loggedIn)

    };


    const deleteComment = (id) => {
        dispatch(DeleteTheCom(id));

    };


    const deletePost = (id) => {
        dispatch(DeleteThePost(id)).then((response) => {
            history.push("/");});
    };

    useEffect(() => {
        dispatch(fetchAllComms())
    }, []);




    let history = useHistory();




    return (
        <div className="postPage">

            <div className="PostDetail">
                <div className="Image">
                <img src={PostById.image?.startsWith('http') ? PostById.image : process.env.REACT_APP_SERVER_URL + PostById.image} />
                </div>
                <div className="Content">
                    <div className="title">
                        {" "}
                        {PostById.title} / by @{PostById.author}
                    </div>
                    <div className="description">{PostById.description}</div>
                    <div className="footer">
                        {loggedIn.username === PostById.author && (
                            <button
                            
                                onClick={() => {
                                    deletePost(PostById.id);
                                }}
                            >
                                {" "}
                                Delete Post
                            </button>
                        )}
                    </div>
                </div>
            </div>

            <div className="rightSide">
                <div className="addCommentContainer">
                    <input
                        type="text"
                        placeholder="Comment..."
                        autoComplete="off"
                        value={description}
                        onChange={(event) => {
                            setNewComment(event.target.value);
                        }}
                    />
                    <button onClick={postingCom}> Add Comment</button>
                </div>
                <div className="listOfComments">
                    {allcomentary.map((comment, key) => {
                        return (
                            <div key={key} className="comment">
                                <label> Comment n° {comment.id} : </label>
                                {comment.description}
                                <label> By user: {comment.author}</label>
                                
                                {loggedIn.username === comment.author && (
                                    <button
                                        onClick={() => {
                                            deleteComment(comment.id);
                                            console.log(comment.id, " was deleted")
                                        }}
                                    >
                                        X
                                    </button>
                                )}
                            </div>
                        );
                    })}
                </div>
            </div>
        </div>
    );
}

export default UploadDetails;

import React, { useState } from "react";
import "./Login.css";
import Axios from "axios";
import { useDispatch, useSelector } from 'react-redux';


import { useHistory } from "react-router-dom";
import { loginWithCredentials } from "../../stores/User";

function Login() {

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();
  const feedback = useSelector(state => state.userStore.loginFeedbackState);

  let history = useHistory();



  const [errorMessage, setErrorMessage] = useState("");


  const loginOn = () => {
    console.log(username, password);

    dispatch(loginWithCredentials({
      username: username,
      password: password,
    })).then((response) => {
      history.push("/");
    });;
    localStorage.setItem('username', username);

  }




  return (
    <div className="Login">
      <h1>Login</h1>
      <div className="LoginForm">
        <input
          type="text"
          placeholder="Username..."
          onChange={(event) => {
            setUsername(event.target.value);
          }}
        />
        <input
          type="password"
          placeholder="Password..."
          onChange={(event) => {
            setPassword(event.target.value);
          }}
        />
        <button onClick={loginOn}>Login</button>
        <h1 style={{ color: "red" }}>{errorMessage} </h1>
      </div>

      <ul class="bg-bubbles">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>
    </div>
  );
}

export default Login;

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import "./Profile.css";
import { fetchPostByUser } from "../../stores/PostsByUsers";

 

function Profile() {
  const dispatch = useDispatch();
  const postByUsers = useSelector((state) => state.uploadsStore.uploadsState);

  const loggedIn = useSelector(state => state.userStore.user);

  useEffect(() => {
    if(loggedIn){    dispatch(fetchPostByUser(loggedIn.id))
  }

  }, [loggedIn]);


  return (
    <div className="Profile">
      <h1>Welcome to your profile {localStorage.getItem("username")}.</h1>
      {postByUsers.map((val, key) => {
        return (
          <div className="Post">
              <img src={val.image?.startsWith('http') ? val.image : process.env.REACT_APP_SERVER_URL + val.image} />

            <div className="Content">
              <div className="title">
                {" "}
                {val.title} / by @{val.author}
              </div>
              <div className="description">{val.description}</div>
            </div>
            <div className="Engagement">{val.likes}</div>
          </div>
        );
      })}
    </div>
  );
}

export default Profile;

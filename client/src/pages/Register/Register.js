import React, { useState } from "react";
import "./Register.css";
import { useDispatch, useSelector } from "react-redux";
import { register } from "../../stores/User";


import { useHistory } from "react-router-dom";

export default function Register() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();
  const feedback = useSelector(
    (state) => state.userStore.registerFeedbackState
  );

  let history = useHistory();


  const registerOnclick = () => {
    dispatch(register({
      username: username,
      password: password,
    })).then((response) => {
      history.push("/login");
    });
    console.log("register front", username, password);
  };

  return (
    <div className="Register">
      {feedback && <p>{feedback}</p>}
      <h1>Registration</h1>
      <div className="RegisterForm">
        <input name="username" type="text" placeholder="Username..." onChange={(event) => {
          setUsername(event.target.value);
        }}
        />
        <input name="password" type="password" placeholder="Password..." onChange={(event) => {
          setPassword(event.target.value);
        }}
        />
        <button onClick={registerOnclick}>Register</button>
      </div>
      <ul class="bg-bubbles">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>
    </div>
  );
}

import React, { useEffect, useState } from "react";
import "./Home.css";

import { fetchAllPosts } from "../../stores/AllPosts";
import { useDispatch, useSelector } from "react-redux";
import { likePost } from "../../stores/Likes";
import { message, Button, Space } from 'antd';
import 'antd/dist/antd.css';
import { PlusOutlined } from '@ant-design/icons';
import { Rate } from 'antd';


import { HeartOutlined } from '@ant-design/icons';
import { useHistory } from "react-router-dom";

import { FaStar } from 'react-icons/fa'
import { fetchAllRatingsPerUser, postingRating } from "../../stores/AllRatings";



function Home() {
  let history = useHistory()

  const [rating, setRating] = useState(null);
  const [hover, setHover] = useState(null);

  const warning = () => {
    message.warning({
      content: 'Login or register to like.',
      className: 'custom-class',
      style: {
        marginTop: '40vh',
      },
    });
  };

  const dispatch = useDispatch();
  const allPosts = useSelector((state) => state.uploadsTotalStore.uploadsTotalState);
  const liking = useSelector((state) => state.likeStore.likeState);
  const loggedIn = useSelector(state => state.userStore.user);
  const RatingById = useSelector((state) => state.ratingStore.ratingState);



  useEffect(() => {
    dispatch(fetchAllPosts());

  }, []);

  useEffect(() => {
    dispatch(fetchAllRatingsPerUser(loggedIn.id));

  }, [loggedIn.id]);




  const postARating = (rating, postId) => {

    /*     console.log(loggedIn.id);
     */
    console.log(RatingById)
    dispatch(postingRating(

      {
        ratingNumber: rating,
        users_id: loggedIn.id,
        uploads_id: postId
      }
    ));
  };


  const postALike = (postId) => {

    // console.log(loggedIn.id);

    dispatch(likePost(
      loggedIn.id,
      postId,
    ))
  };

  return (
    <div className="Home">
      {allPosts.map((val, key) => {
        return (
          <div className="Post">
            <div className="Image">
              <img src={val.image.startsWith('http') ? val.image : process.env.REACT_APP_SERVER_URL + val.image} />
            </div>
            <div className="Content">
              <div className="title">
                {" "}
                {val.title} / by @{val.author}
              </div>
              <div className="description">{val.description}</div>
            </div>



            <div className="emoticones">

              <PlusOutlined className="Engagement" style={{ fontSize: '250%'}} onClick={() => {
                history.push(`/uploadDetails/${val.id}`);
              }} />
              {/* 
            <Rate className="rate" onClick={() => {
              
            }}/> */}

              <div className="canRate">
                <p> Your rating is {rating}</p>
                <p> Average note is </p>


                {[...Array(5)].map((star, i) => {

                  const ratingValue = i + 1;
                  return (
                    <label>
                      <input
                        type="radio"
                        name="rating"
                        value=""
                        onClick={() => {
                          setRating(ratingValue, val.id);
                        }}
                      />

                      <FaStar

                        className="star"
                        color={ ratingValue <= (hover || rating) ? "#496595" : "#e4e5e9"}
                        size={50}
                        onMouseEnter={() => setHover(ratingValue)}
                        onMouseLeave={() => setHover(null)}
                        onClick={() => {
                          postARating(ratingValue, val.id);
                        }}
                      />


                    </label>
                  );
                })}

                {/* <i class="fas fa-star"></i>
 */}

              </div>

              {loggedIn ? (
                <div className="Engagement">
                  <HeartOutlined
                    id="likeButton"
                    onClick={() => {
                      postALike(val.id);
                    }}
                  />
                  {val.likes}
                </div>

              ) : (
                <Space>
                  <div className="Engagement">
                    <HeartOutlined id="likeButton"
                      onClick={warning} />


                    {val.likes}
                  </div>


                </Space>
              )}
            </div>

          </div>
        );
      })}
    </div>
  );
}

export default Home;

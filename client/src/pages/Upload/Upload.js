import React, { useEffect, useState } from "react";
import "./Upload.css";

import { uploadPost } from "../../stores/UploadPosts";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { loadPartialConfig } from "@babel/core";



function Upload() {
  let history = useHistory();


  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");

  const dispatch = useDispatch();
  const uploadNewPost = useSelector(
    (state) => state.uploadPostStore.postState
  );

  const upload = () => {
    dispatch(uploadPost({ 
      title: title,
      description: description,
      image: image

    })).then((response) => {
      history.push("/");
    });
    console.log(title, description, image);
  };


  /*   const upload = () => {
      console.log(title, description, image);
      const formData = new FormData();
      formData.append("title", title);
      formData.append("description", description);
      formData.append("image", image);
  
      axios.post(
        `http://localhost:3001/api/v1/upload`,
        formData
      ).then((response) => {
        const fileName = response.data.public_id;
        history.push("/");
      });
  
    }; */


  return (
    <div className="Upload" action="/upload/" method="post" enctype="multipart/form-data">
      <h1>Create A Post</h1>
      <div className="UploadForm">
        <input
          type="text"
          placeholder="Title..."
          onChange={(event) => {
            setTitle(event.target.value);
          }}
        />
        <input
          type="text"
          placeholder="Description..."
          onChange={(event) => {
            setDescription(event.target.value);
          }}
        />

        <input type="file" name="image" onChange={(event) => setImage(event.target.files[0])} />
        <button onClick={upload}>Upload</button>
      </div>

      <ul class="bg-bubbles">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>
    </div>
  );
}

export default Upload;

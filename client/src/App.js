import { BrowserRouter as Router, Route } from "react-router-dom";
import { Switch } from "react-router-dom"
import Home from "./pages/Home/Home";
import Navbar from "./components/Navbar";
import Register from "./pages/Register/Register";
import Login from "./pages/Login/Login";
import Upload from "./pages/Upload/Upload";
import Profile from "./pages/Profile/Profile";
import { loginWithToken } from "./stores/User";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import UploadDetails from './pages/UploadDetails/UploadDetails'
import { Spin, Space } from 'antd';
import 'antd/dist/antd.css';
import './App.css'


function App() {
  const loggedIn = useSelector(state => state.userStore.user);

  const dispatch = useDispatch();

  useEffect(() => {

    dispatch(loginWithToken());

  }, []);
  return (

    <>
      <Navbar />
      <Router>
        <Switch>
        <Route exact path="/"
          >
            {loggedIn ?
              <Home/>
              :
              <div className="container-loading"       style={{
                
              }}>

                <Space size="middle">
                  <Spin tip="Loading..." size="large" />
                </Space>

              </div>}
          </Route>          <Route path="/register" exact render={() => <Register />} />
          <Route path="/login" exact render={() => <Login />} />
          <Route path="/upload" exact render={() => <Upload />} />
          <Route path="/profile" exact render={() => <Profile />} />

          <Route path="/uploadDetails/:id"
          >
            {loggedIn ?
              <UploadDetails></UploadDetails>
              :
              <div className="container-loading"       style={{
                
              }}>

                <Space size="middle">
                  <Spin tip="Loading..." size="large" />
                </Space>

              </div>}
          </Route>


        </Switch>
      </Router>
    </>
  );
}

export default App;

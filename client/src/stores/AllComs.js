import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    commentsTotalState: []
};
  
const AllComs = createSlice({
    name: 'All comments',
    initialState,
    reducers: {
        setAllComments(state, { payload }) {
            state.commentsTotalState = payload;
        },
        deleteAcomment(state, { payload }) {
            state.commentsTotalState = state.commentsTotalState.filter(item => item.id != payload)
        }
    }
});

export const { setAllComments, deleteAcomment } = AllComs.actions;

export default AllComs.reducer;

export const fetchAllComms = () => async (dispatch) => {
    try {
        const response = await axios.get(`https://blog-app-back-hedwig.herokuapp.com/api/v1/comment`);
        console.log(response.data.data);

        dispatch(setAllComments(response.data.data));
    } catch (error) {
        console.log(error);
        console.log(error.response.data)
    }
}

export const DeleteTheCom = (id) => async (dispatch) => {
    try {
        const response = await axios.delete(`https://blog-app-back-hedwig.herokuapp.com/api/v1/comment/${id}`); 
        console.log(response.data)
        dispatch(deleteAcomment(id));
    } catch (error) {
        console.log(error);
        console.log(error.response.status) // 401
        console.log(error.response.data)
    }
}

import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { fetchAllComms } from "./AllComs";
import { fetchAllPosts } from "./AllPosts";


const initialState = {
    ratingState: []
};

const AllRatings = createSlice({
    name: 'ratings',
    initialState,
    reducers: {
        ratingPost(state, { payload }) {
            state.ratingState.push(payload);
        },
        setRatingPerUser(state, {payload}) {
            state.ratingState = payload;
        }
    }
});

export const { ratingPost , setRatingPerUser} = AllRatings.actions;

export default AllRatings.reducer;

export const postingRating = ({ratingNumber, users_id, uploads_id}) => async (dispatch) => {


    
    try { 

        const response = await axios.post(`https://blog-app-back-hedwig.herokuapp.com/api/v1/rating`, {ratingNumber, users_id, uploads_id});
        // dispatch(fetchAllPosts());
        dispatch(ratingPost(response.data));
    } catch (error) {
        console.log(error);
        console.log(error.data)
    }
}


export const fetchAllRatingsPerUser = (id) => async (dispatch) => {
    try {
        
        const response = await axios.get(`https://blog-app-back-hedwig.herokuapp.com/api/v1/rating/byUser/${id}`);

        dispatch(setRatingPerUser(response.data.data));

    } catch (error) {
        console.log(error);
        console.log(error.response.data)
    }
}
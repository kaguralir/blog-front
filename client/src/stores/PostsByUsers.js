import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    uploadsState: []
};

const PostsByUsers = createSlice({
    name: 'uploads',
    initialState,
    reducers: {
        setUploads(state, {payload}) {
            state.uploadsState= payload;
        }
    }
});

export const {setUploads} = PostsByUsers.actions;

export default PostsByUsers.reducer;

export const fetchPostByUser = (id) => async (dispatch) => {
    try {
        console.log(id)
        const response = await axios.get(`https://blog-app-back-hedwig.herokuapp.com/api/v1/upload/byUser/${id}`);
        // console.log(localStorage.getItem("username"));
        dispatch(setUploads(response.data)); 
    } catch (error) {
        console.log(error);
        console.log(error.response.status) // 401
        console.log(error.response.data)
    }
}

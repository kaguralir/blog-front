import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { fetchAllComms } from "./AllComs";


const initialState = {
    comState: []
};

const PostCom = createSlice({
    name: 'comments',
    initialState,
    reducers: {
        commentPost(state, { payload }) {
            state.comState.push(payload);
        }
    }
});

export const { commentPost } = PostCom.actions;

export default PostCom.reducer;

export const postCom = ({description, author, users_id, uploads_id}) => async (dispatch) => {


    
    try { 
/*         const formData = new FormData();
        formData.append("description", description);

        formData.append("author", author);

        formData.append("uploads_id", uploads_id); */
        const response = await axios.post(`https://blog-app-back-hedwig.herokuapp.com/api/v1/comment`, {description, author, users_id, uploads_id});
        dispatch(fetchAllComms());
        dispatch(commentPost(response.data));
    } catch (error) {
        console.log(error);
    }
}

import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";


const initialState = {
    postState: []
};

const UploadPosts = createSlice({
    name: 'uploads',
    initialState,
    reducers: {
        addPost(state, { payload }) {
            state.postState.push(payload);
        }
    }
});

export const { addPost } = UploadPosts.actions;

export default UploadPosts.reducer;

export const uploadPost = ({title,description,image}) => async (dispatch) => {


    
    try {
        const formData = new FormData();
        formData.append("title", title);
        formData.append("description", description);
        formData.append("image", image);
        const response = await axios.post(`https://blog-app-back-hedwig.herokuapp.com/api/v1/upload`, formData );
        dispatch(addPost(response.data));
    } catch (error) {
        console.log(error);
        console.log(error.response.status) // 401
        console.log(error.response.data)
    }
}

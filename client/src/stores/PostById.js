import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    uploadsByIdState: []
};

const PostsById = createSlice({
    name: 'uploads',
    initialState,
    reducers: {
        setUploads(state, {payload}) {
            state.uploadsByIdState= payload;
        }
    }
});

export const {setUploads} = PostsById.actions;

export default PostsById.reducer;

export const fetchPostById = (id) => async (dispatch) => {
    try {
        const response = await axios.get(`https://blog-app-back-hedwig.herokuapp.com/api/v1/upload/byPost/${id}`); 
        console.log("post by id is ",response.data)
        dispatch(setUploads(response.data));
    } catch (error) {
        console.log(error);
        console.log(error.response.status) // 401
        console.log(error.response.data)
    }
}

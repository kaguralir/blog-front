import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import AllPosts, { fetchAllPosts, setAllPosts } from "./AllPosts";

const initialState = {
    likeState: []
};

const Likes = createSlice({
    name: 'All likes',
    initialState,
    reducers: {
        setLikes(state, { payload }) {
            state.likeState = payload;
        }
    
    }
});

export const { setLikes } = Likes.actions;

export default Likes.reducer;


export const likePost = (users_id, uploads_id) => async (dispatch) => {

    try {
        console.log(users_id, uploads_id)
        const { data } = await axios.post(`https://blog-app-back-hedwig.herokuapp.com/api/v1/upload/like`, {
            users_id,
            uploads_id})
        dispatch(fetchAllPosts());
        dispatch({ type: setLikes, payload: data });
    } catch (error) { 
        console.log(error);
    }
};


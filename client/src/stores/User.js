import { createSlice } from "@reduxjs/toolkit";
import { AuthService } from "../services/AuthService";
import { addPost } from './UploadPosts'

const User = createSlice({
  name: "auth",
  initialState: {
    user: null,
    loginFeedbackState: "",
    registerFeedbackState: "",
  },
  reducers: {
    login(state, { payload }) {
      state.user = payload;
    },
    logout(state) {
      state.user = null;
      localStorage.removeItem("token");
    },
    updateLoginFeedback(state, { payload }) {
      state.loginFeedbackState = payload;
    },
    updateRegisterFeedback(state, { payload }) {
      state.registerFeedbackState = payload;
    },
  },
});

export const { login, logout, updateLoginFeedback, updateRegisterFeedback } =
  User.actions;

export default User.reducer;

export const loginWithToken = () => async (dispatch) => {
  const token = localStorage.getItem("token");

  if (token) {
    try {
      const user = await AuthService.fetchAccount();
      console.log(user)
      dispatch(login(user));
    } catch (error) {
      dispatch(logout());
    }
  }
};

export const register = (body) => async (dispatch) => {
  console.log(body);
  try {
    const { user, token } = await AuthService.register(body);
    localStorage.setItem("token", token);

    dispatch(updateRegisterFeedback("Registration successful"));
    dispatch(login(user));

    console.log(user);
  } catch (error) {
    console.log(error)
    dispatch(updateRegisterFeedback(error.response.data.error));
  }
};

export const loginWithCredentials = (credentials) => async (dispatch) => {
  try {
    const user = await AuthService.login(credentials);
    dispatch(updateLoginFeedback("Login successful"));
    console.log(updateLoginFeedback.data);
    // dispatch(addPost(user));
    dispatch(login(user));
  } catch (error) {
    dispatch(updateLoginFeedback("Email and/or password incorrect"));
  }
};

import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    uploadsTotalState: []
};
  
const AllPosts = createSlice({
    name: 'All uploads',
    initialState,
    reducers: {
        setAllPosts(state, { payload }) {
            state.uploadsTotalState = payload;
        },
        deleteApost(state, { payload }) {
            state.uploadsTotalState = state.uploadsTotalState.filter(item => item.id != payload)
        }
    }
});

export const { setAllPosts , deleteApost} = AllPosts.actions;

export default AllPosts.reducer;

export const fetchAllPosts = () => async (dispatch) => {
    try {
        const response = await axios.get(`https://blog-app-back-hedwig.herokuapp.com/api/v1/upload`);
        console.log(response.data.data);

        dispatch(setAllPosts(response.data.data));
    } catch (error) {
        console.log(error);
    }
}


export const DeleteThePost = (id) => async (dispatch) => {
    try {
        const response = await axios.delete(`http://localhost:3001/api/v1/upload/${id}`); 
        console.log(response.data)
        dispatch(deleteApost(response.data));
    } catch (error) {
        console.log(error);
        console.log(error.response.status) // 401
        console.log(error.response.data)
    }
}


import { configureStore } from "@reduxjs/toolkit";
import AllComs from "./AllComs";
import AllPosts from "./AllPosts";
import AllRatings from "./AllRatings";
import Likes from "./Likes";
import PostById from "./PostById";
import PostCom from "./PostCom";
import PostsByUsers from "./PostsByUsers";
import UploadPosts from "./UploadPosts";
import User from "./User";

export const store = configureStore({
  reducer: {
    uploadsStore: PostsByUsers,
    uploadsTotalStore: AllPosts,
    likeStore: Likes,
    uploadPostStore: UploadPosts,
    userStore: User,
    uploadsByIdStore: PostById,
    postingCommentary: PostCom,
    gettingAllCommentary: AllComs,
    deletingApost: AllPosts,
    deletingACom : AllComs,
    ratingStore: AllRatings
  },
});

import React, { useEffect, useState } from "react";
import "./Home.css";

import { useDispatch, useSelector } from "react-redux";
import { likePost } from "../../stores/Likes";
import { message, Button, Space } from 'antd';
import 'antd/dist/antd.css';
import { PlusOutlined } from '@ant-design/icons';
import { Rate } from 'antd';


import { HeartOutlined } from '@ant-design/icons';

import { FaStar } from 'react-icons/fa'
import { fetchAllRatingsPerUser, postingRating } from "../../stores/AllRatings";



function Ratings() {

    const [rating, setRating] = useState(null);



    const dispatch = useDispatch();
    const allPosts = useSelector((state) => state.uploadsTotalStore.uploadsTotalState);
    const liking = useSelector((state) => state.likeStore.likeState);
    const loggedIn = useSelector(state => state.userStore.user);
    const RatingById = useSelector((state) => state.ratingStore.ratingState);




    const allPosts = useSelector((state) => state.uploadsTotalStore.uploadsTotalState);


    const postARating = (rating, postId) => {

        /*     console.log(loggedIn.id);
         */
        console.log(RatingById)
        dispatch(postingRating(

            {
                ratingNumber: rating,
                users_id: loggedIn.id,
                uploads_id: postId
            }
        ));
    };



    return (
        <div >
            {allPosts.map((val, key) => {
                return (
                    <div className="canRate">
                        <p> Your rating is {rating}</p>
                        <p> Average note is </p>


                        {[...Array(5)].map((star, i) => {

                            const ratingValue = i + 1;
                            return (
                                <label>
                                    <input
                                        type="radio"
                                        name="rating"
                                        value=""
                                        onClick={() => {
                                            setRating(ratingValue, val.id);
                                        }}
                                    />

                                    <FaStar

                                        className="star"
                                        color={ratingValue <= (hover || rating) ? "#496595" : "#e4e5e9"}
                                        size={50}
                                        onMouseEnter={() => setHover(ratingValue)}
                                        onMouseLeave={() => setHover(null)}
                                        onClick={() => {
                                            postARating(ratingValue, val.id);
                                        }}
                                    />


                                </label>
                            );
                        })}

                    </div>





                );
            })}
        </div>
    );
}

export default Home;

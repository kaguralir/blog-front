

import React, { useState } from "react";
import Logo from "../assets/bird-logo.png";
import Logos from "../assets/iguana.svg";

import { Link, NavLink } from "react-router-dom";
import ReorderIcon from "@material-ui/icons/Reorder";
import "./Navbar.css";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../stores/User";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'




function Navbar() {
  const [click, setClick] = React.useState(false);

  const loggedIn = useSelector(state => state.userStore.user);
  const dispatch = useDispatch();
  const handleClick = () => setClick(!click);
  const Close = () => setClick(false);

  return (
    <div>
      <div className={click ? "main-container" : ""} onClick={() => Close()} />
      <nav className="navbar" onClick={(e) => e.stopPropagation()}>
        <div className="nav-container">
          <div className="nav-logo" className="logos">
          <a href="/" className="nav-links" >Jungle is beautiful</a>
          <i class="fas fa-leaf"></i>

</div>

          <ul className={click ? "nav-menu active" : "nav-menu"}>




            {loggedIn ? (
              <>
                <li className="nav-item"> <a href="/upload" className="nav-links" >Upload</a></li>
                <li className="nav-item"><a href="/profile" className="nav-links" >Profile</a></li>
                <li className="nav-item"> <a href="/" className="nav-links" onClick={() => dispatch(logout())}>Logout</a></li>

              </>
            ) : (
              <>
                <li className="nav-item"><a href="/register" className="nav-links">Register</a></li>
                <li className="nav-item"><a href="/login" className="nav-links">Login</a></li>
              </>
            )}
          </ul>
          <div className="nav-icon" onClick={handleClick}>
            <i className={click ? "fa fa-times" : "fa fa-bars"}></i>
          </div>
        </div>
      </nav>
    </ div>
  );
}
export default Navbar;